<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
    	$faker = Faker\Factory::create();

    	$limit = 40;

    	App\User::create([
    		'username' => 'admin',
    		'email' => 'hsiehminyan@gmail.com',
    		'name' => 'Min Hsieh',
    		'password' => bcrypt('123456'),
            'role_id' => 1
    	]);

        for ($i = 0; $i < $limit; $i++) {
            App\User::create([
	        	'username' => $faker->userName,
	        	'email' => $faker->unique()->email,
	        	'name' => $faker->firstName,
	        	'password' => bcrypt('123456'),
                'role_id' => rand(2,5)
	        ]);
        }
    }
}
