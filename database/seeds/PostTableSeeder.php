<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->truncate();
        $faker = Faker\Factory::create();

    	$limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            App\Post::create([
	    		'title' => $faker->realText(30),
	    		'content' => $faker->realText(),
	    		'user_id' => rand(1,40),
	    	]);
        }
    }
}
