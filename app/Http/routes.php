<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('login',['uses' => 'Auth\LoginController@index' , 'as' => 'login']);
Route::post('login',['uses' => 'Auth\LoginController@login']);
Route::get('logout',['uses' => 'Auth\LoginController@logout']);

Route::group(['middleware' => 'auth'],function () {	
	Route::get('/', function () {
	    return view('welcome');
	});

	Route::group(['prefix' => 'post'],function() {
		Route::get('/' , ['uses' => 'PostController@index']);
		Route::get('create' , ['uses' => 'PostController@create']);
		Route::post('store' , ['uses' => 'PostController@store']);
		Route::get('show/{id}' , ['uses' => 'PostController@show']);
		Route::post('edit' , ['uses' => 'PostController@edit']);
		Route::get('destroy/{id}' , ['uses' => 'PostController@destroy']);
	});

	Route::group(['prefix' => 'role' , 'middleware' => 'roles'],function() {
		Route::get('root' , ['uses' => 'PostController@index' , 'roles' => 'root']);
	});

});
