<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

class LoginController extends Controller
{
    public function index()
    {
    	if(Auth::check()){
    		return 'already login';
    	}
    	return view('auth.login');
    }

    public function login(Request $req)
    {
    	if(Auth::attempt(['username' => $req->username , 'password' => $req->password])){
    		return 'login success';
    	}else{
    		return redirect()->action('Auth\LoginController@index')->with('error','帳號密碼錯誤，請重新再試。');
    	}
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect()->action('Auth\LoginController@index')->with('success','登出成功！');
    }
}
