<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Validator;

use App\User;
use App\Post;

class PostController extends Controller
{
    public function index()
    {
    	$user = Auth::user();
    	$posts = $user->posts;
    	return view('post.index',['posts' => $posts]);
    }

    public function create()
    {
    	return view('post.create');
    }

    public function store(Request $req)
    {
    	$validator = Validator::make($req->all(),Post::$rules,Post::$messages);
    	if ($validator->fails()) {
            return redirect('post/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        Post::create([
        	'title' => $req->title,
        	'content' => $req->content,
        	'user_id' => Auth::user()->id
        	]);
        return redirect()->action('PostController@index')->with('success','成功新增一筆資料');
    }

    public function show($id)
    {
    	$post = Post::find($id);
    	return view('post.edit' ,['post' => $post]);
    }

    public function edit(Request $req)
    {
    	$validator = Validator::make($req->all(),Post::$rules,Post::$messages);
    	if ($validator->fails()) {
            return redirect()
            			->action('PostController@show',['id'=>$req->id])
                        ->withErrors($validator)
                        ->withInput();
        }

        Post::find($req->id)
        	->update([
        		'title' => $req->title,
        		'content' => $req->content
        	]);

        return redirect()->action('PostController@index')->with('success','成功編輯一筆資料');
    }

    public function destroy($id)
    {
    	if(Post::find($id)->user_id != Auth::user()->id){
    		return redirect()->action('PostController@index')->with('error','you cant delete this post.');
    	}
    	Post::destroy($id);
    	return redirect()->action('PostController@index')->with('success','delete this post success');
    }
}
