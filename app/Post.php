<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	public static $rules = [
	        'title'     => 'required|min:2|max:30',
	    ];

	public static $messages = [
            'title.required' => "Post's title is required.",
            'title.min' => "title at least 2 chars.",
            'title.max' => "title cant more than 30 chars"
        ];

    protected $fillable = [
    	'title',
    	'content',
    	'user_id'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
